using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{

    Rigidbody rb;
    [SerializeField]
    float thrustSpeed = 1000f;
    [SerializeField]
    float sideThrustSpeed = 100f;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        ProcessThrust();
        ProcessRotation();
    }

    void ProcessRotation()
    {
        if (Input.GetKey(KeyCode.A))
        {
            Rotate(sideThrustSpeed);
        }
        else if (Input.GetKey(KeyCode.D))
        {
            Rotate(-sideThrustSpeed);
        }
    }

    void Rotate(float rotationThisFrame)
    {
        //Disable Physics System Rotation To Prevent Collision Bugs.
        rb.freezeRotation = true;
        transform.Rotate(Vector3.forward * rotationThisFrame * Time.deltaTime);
        rb.freezeRotation = false;
    }

    void ProcessThrust()
    {
        if (Input.GetKey(KeyCode.Space))
        {
            rb.AddRelativeForce(Vector3.up *  thrustSpeed * Time.deltaTime);
        }      
    }
}
